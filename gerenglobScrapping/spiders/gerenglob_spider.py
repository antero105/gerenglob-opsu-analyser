from scrapy import Spider
from GerenglobscrappingItem import GerenglobscrappingItem
import json


class gerenglobSpider(Spider):

    name = "gerenglobSpider"
    allowed_domains = ["oeu.opsu.gob.ve"]
    start_urls = []
    def __init__(self):
		super(gerenglobSpider, self).__init__()
		
		with open('listCreator/data.json') as json_file:
			json_data = json.load(json_file)
			for value in json_data:
				self.start_urls.append(value['url'])
		
		

	
    def parse(self, response):
    	item = GerenglobscrappingItem()
        professions =response.selector.xpath('//table[@bgcolor="#C0C0C0" and @width="100%" and @border="0" and @cellpadding ="5" and @cellspacing="1"]/tbody/tr/td/table/tbody/tr[@class = "tb_celda_impar" or @class = "tb_celda_par"]/td[2]/a/text()')
        name = response.selector.xpath('//span[@class="texgris"]/text()')
        direction = response.selector.xpath('//table[@id = "tabla_contenido"]/tbody/tr[5]/td/table/tbody/tr[1]/td/text()')
       	uni_type = response.selector.xpath('//span[@class="texrojo"]/text()')

        item['uni_tipo'] = uni_type.extract()
       	item["profesiones"] = professions.extract()
       	item["nombre"] = name.extract()
       	item['estado'] = direction.extract()

       	return item



