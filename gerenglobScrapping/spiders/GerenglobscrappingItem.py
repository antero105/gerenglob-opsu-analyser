from scrapy.item import Item,Field

class GerenglobscrappingItem(Item):
	"""docstring for gerenglobSpiderItems"""
	profesiones = Field()
	nombre = Field()
	estado = Field()
	contacto = Field()
	uni_tipo = Field()