# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymongo
from scrapy.exceptions import DropItem
from scrapy.conf import settings
from scrapy import log
class GerenglobscrappingPipeline(object):
    def __init__(self):
    	
    	connection = pymongo.Connection(settings['MONGODB_SERVER'],settings['MONGODB_PORT'])
    	db = connection[settings['MONGODB_DB']]
    	self.collection = db[settings['MONGODB_COLLECTION']] 
    	self.collection.drop()
   	

    def process_item(self, item, spider):
    	
    	
    	item['nombre'] = item['nombre'][0].split('-')[0].strip()
    	if item['estado'][7].strip() != '':
    		item['contacto'] = [item['estado'][7]]
    	
    	item['estado'] = item['estado'][2].split(':')[1].strip()
    	
    	#item['profesiones'] = [l for l in item['profesiones']]
    	
        item['uni_tipo']=item['uni_tipo'][0].split(':')[1].strip().split(' ')[1]
    	
        
        for i in item['profesiones']:
            self.collection.save({'nombre':item['nombre'],'estado':item['estado'],'tipo':item['uni_tipo'],'profesion':i})
        #self.collection.save(dict(item))

    	
  

